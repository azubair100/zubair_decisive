
For number 1, after reading the task, the only way I could have generated possible strings was through recursion. I first find all the ambiguous characters inside the params. I store them and their indices in a hash.

I have created another method which does the actual recursion. 

After I have generated the possible combinations of ambiguous characters. I replace those specific characters inside the string and I make a search. 


For example if a token A8REI26 is sent as a param

There are 2 ambiguous characters here 8 and I. Their replacements are B and 5.

So the code will store like this {'8' => 1, 'I' => [1, L]} # keys are the ambiguous letters that are present in the string and values are their indices in the param.

(This step may be redundant and need to think of a better way to do this)
After I make an array of the keys and values. ['8', 'I'] and [1, 4]

Then I call the method possible_token_generator and generate possible combinations by changing the letters with the alternatives. I pass ['8', 'I'] as the params

The idea behind the method is that 

                               8 I
                              /     \
                             /       I   # returns [I, L, 1] back to level 1  gives you 81, 8L, 8I
                            /      /   \
                           /      1      L 
                          B # After 8 finishes you get B1, BL, BI

The combinations you get are 8 I, 8 1, 8 l, B I , BL , B 1 . These combinations are then stored in an array which 
is traversed through a each loop where I use the index array to insert them back into the original params.

The combinations produced for this example will be

A8REI26 (original) 8 I, 
A8RE126     =>     8 1, 
A8REL26     =>     8 L, 
ABRE126     =>     B 1, 
ABREL26     =>     B L, 
ABREI26     =>     B I, 

Then I search them if there is any match.

There is a scalability issue here. Best case scenario I find the match in the first combination, worst case I find in the last combination which will be bad if a string only has ambiguous characters.
   
For number 2, I use LIKE queries to by skipping 2 characters from the token.

For example,

if token is A423R21.

The search goes like this

SELECT COUNT(*) FROM "urls" WHERE (token LIKE '%423R2%' OR token LIKE '%23R21%' OR (token LIKE '423R%' AND token LIKE '%1') OR (token LIKE '423%' AND token LIKE '%21') OR (token LIKE '42%' AND token LIKE '%R21') OR (token LIKE '4%' AND token LIKE '%3R21') OR
       (token LIKE 'A%' AND token LIKE '%23R2%') OR (token LIKE 'A%' AND token LIKE '%23R%' AND token LIKE '%1') OR (token LIKE 'A%' AND token LIKE '%23%' AND token LIKE '%21') OR (token LIKE 'A%' AND token LIKE '%2%' AND token LIKE '%R21') OR (token LIKE 'A%' AND token LIKE '%3R21') OR
       (token LIKE 'A4%' AND token LIKE '%3R2%') OR (token LIKE 'A4%' AND token LIKE '%3R%' AND token LIKE '%1') OR (token LIKE 'A4%' AND token LIKE '%3%' AND token LIKE '%21') OR (token LIKE 'A4%' AND token LIKE '%R21') OR
       (token LIKE 'A42%' AND token LIKE '%R2%') OR (token LIKE 'A42%' AND token LIKE '%R%' AND token LIKE '%1') OR (token LIKE 'A42%' AND token LIKE '%21') OR
       (token LIKE 'A423%' AND token LIKE '%2%') OR (token LIKE 'A423%' AND token LIKE '%1') OR
        token LIKE 'A423R%')


The query always searches by skipping 2 chars and ensures the token generated is different by at least 2 chars.