class Url < ActiveRecord::Base
  validates :token, presence: true, uniqueness: true
  validates :long_url, presence: true, url: true

  before_validation :set_token, on: :create


  # This hash will store all the ambiguous chars as keys and their replacement as an array of strings
  AMBIGUOUS_HASH = {

      "1"  => %w{I L},
      "I"  => %w{1 L},
      "L"  => %w{1 I},

      "5"  => %w{S},
      "S"  => %w{5},

      "0"  => %w{O D},
      "O"  => %w{O D},
      "D"  => %w{O 0},

      "8"  => %w{B},
      "B"  => %w{8},

      "U"  => %w{V W},
      "V"  => %w{U W},
      "W"  => %w{U V},

  }

  def to_param
    token
  end

  def self.possible_token_generator(token)
    return_val = []
    #base case
    #if last character of the string is reached, return the character alongside with its replacement character/s
    if token.length == 1
      # The maximum replacement character a character can has is 2.
      return_val = [token.first, AMBIGUOUS_HASH[token.first].first]
      return_val << AMBIGUOUS_HASH[token.first].second if AMBIGUOUS_HASH[token.first].second.present?

      return return_val

    else
      # last character hasn't been reached yet so call the action again by skipping the first character
      return_val =  Url.possible_token_generator(token[1..token.length])

      # if there is a value present make a combination and return it and move forward in the stack
      if return_val.present?
        current_char_array = [token[0], AMBIGUOUS_HASH[token[0]].first]
        current_char_array << AMBIGUOUS_HASH[token[0]].second if AMBIGUOUS_HASH[token[0]].second.present?
        temporary_array = []
        for current_char in current_char_array
          for char in return_val
            temporary_array << current_char + char
          end
        end

        #empty return val
        return_val = []
        return_val = temporary_array
      end

      return  return_val
    end

  end


  #this action searches for any ambiguous letter present in the token
  def self.find_ambiguous_letters(token)
    #I decided to create a hash to find where in the token the ambiguous letters exist so I created a hash to track it
    problem_chars = {}
    token.split("").each do |char|
      if AMBIGUOUS_HASH.include? char
        problem_chars["#{char}"] = token.index(char)
      end
    end
    return problem_chars
  end

  #incoming url
  def self.with_token(token)
    #search exactly the params appear
    result = Url.where(token: token).first
    #if first search was blank, look for ambiguous chars

    #if initial search was blank we find the closest relative
    if result.blank?
      #find which ambiguous chars are present alongside their indices and store them in a hash
      problem_hash = Url.find_ambiguous_letters(token)

      # I need an array for the sequences and the chars
      if problem_hash.present?
        problem_array_sequence = problem_hash.values
        problem_array = problem_hash.keys
      end

      # In this array, I store combinations of replaced characters, which will later be replaced into the main token
      combinations_of_problem_tokens = []

      #if there are any ambiguous letters
      if problem_array.present? and problem_array_sequence.present?

        # we call action that generates all the possible combination
        combinations_of_problem_chars =  Url.possible_token_generator(problem_array) #combo od just problem chars

        # if the combination array has been filled up we move on to the next course of action
        if combinations_of_problem_chars.present?

          # for loop for each combination of ambiguous characters and their replacements
          for chars in combinations_of_problem_chars

            #since we will have to replace the ambiguous characters inside the original token or parameter,
            # we store it in a variable
            str = token

            #now we take the combination string and match them by sequence they should appear in the string
            index_of_char = 0
            chars.split('').each do |char|

              #this variable will store where the characters appear inside the combination
              #index_of_char = chars.index(char)


              #the logic behind the switch statement is that
              #When we select a character from the combination, we have to match it the way its replacement had
              #appeared in the original token/params

              case index_of_char
                when 0
                  str[problem_array_sequence[0]] = char if problem_array_sequence[0].present?
                when 1
                  str[problem_array_sequence[1]] = char if problem_array_sequence[1].present?
                when 2
                  str[problem_array_sequence[2]] = char if problem_array_sequence[2].present?
                when 3
                  str[problem_array_sequence[3]] = char if problem_array_sequence[3].present?
                when 4
                  str[problem_array_sequence[4]] = char if problem_array_sequence[4].present?
                when 5
                  str[problem_array_sequence[5]] = char if problem_array_sequence[5].present?
                when 6
                  str[problem_array_sequence[6]] = char if problem_array_sequence[6].present?

              #case
              end
              index_of_char = index_of_char + 1
            #search per character inside the combination string
            end

            #after a fresh string is generated we search it up


            result = Url.where(token: str).first


            #if anything is found return it, if not continue
            return result if result.present?

          #for loop for generating valid tokens for search
          end

          #if combinations with problem characters were generated
        end

       #if the ambiguous array and ambiguous_sequence is present?
      end

     #if original search was blank
    end

    return result
  end

  private

  #called through callback
  def set_token
    self.token = make_unique_token
  end

  #creates unqie token
  def make_unique_token
    loop do
      token = build_token_value
      return token if token_is_unique?(token)
    end
  end

  #boolean if it is unique
  def token_is_unique?(token)
    unique_by_two_chars(token) == 0
  end

  #builds token
  def build_token_value
    #since there are no restrictions we can use this for random string generation
    SecureRandom.hex.upcase[0..6]
  end

  def unique_by_two_chars(token)

    #I am yet to figure out how to do this dynamically, so I hard coded it
    #The way this thing works is.. at any give LIKE statement it searches by skipping 2 characters from the string
    Url.where(
        'token LIKE ? OR token LIKE ? OR (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR
       (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR
       (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR
       (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR
       (token LIKE ? AND token LIKE ?) OR (token LIKE ? AND token LIKE ?) OR
        token LIKE ?',

        '%' + "#{token[1..5]}" + '%',  '%' + "#{token[2..6]}" + '%', "#{token[1..4]}" + '%', '%' + "#{token[6]}",   "#{token[1..3]}" + '%', '%' + "#{token[5..6]}",   "#{token[1..2]}" + '%', '%' + "#{token[4..6]}",   "#{token[1]}" + '%', '%' + "#{token[3..6]}",
        "#{token[0]}" + '%', '%' + "#{token[2..5]}" + '%',   "#{token[0]}" + '%', '%' + "#{token[2..4]}" + '%', '%' + "#{token[6]}",   "#{token[0]}" + '%', '%' + "#{token[2..3]}" + '%', '%' + "#{token[5..6]}",   "#{token[0]}" + '%', '%' + "#{token[2]}" + '%', '%' + "#{token[4..6]}", "#{token[0]}" + '%', '%' + "#{token[3..6]}",
        "#{token[0..1]}" + '%', '%' + "#{token[3..5]}" + '%',   "#{token[0..1]}" + '%', '%' + "#{token[3..4]}" + '%', '%' + "#{token[6]}",    "#{token[0..1]}" + '%', '%' + "#{token[3]}" + '%', '%' + "#{token[5..6]}",   "#{token[0..1]}" + '%', '%' + "#{token[4..6]}",
        "#{token[0..2]}" + '%', '%' + "#{token[4..5]}" + '%',   "#{token[0..2]}" + '%', '%' + "#{token[4]}" + '%', '%' + "#{token[6]}",    "#{token[0..2]}" + '%', '%' + "#{token[5..6]}",
        "#{token[0..3]}" + '%', '%' + "#{token[5]}" + '%',    "#{token[0..3]}" + '%', '%' + "#{token[6]}",
        "#{token[0..4]}" + '%'
    ).count

  end
  

end







