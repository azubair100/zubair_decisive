class UrlsController < ApplicationController

  def show
    load_url
    redirect_to @url.long_url unless params[:redirect] == "no"
  end

  def new
    build_url
  end

  def create
    build_url(permitted_params)
    save_url or render action: "new", status: :unprocessable_entity
  end

  private

  def load_url
    @url ||= url_scope.with_token(params[:token])
    #errors get redirected check application controller
    raise ActiveRecord::RecordNotFound unless @url
  end

  def build_url(url_params = nil)
    @url ||= url_scope.build
    @url.attributes = url_params if url_params
  end

  def save_url
    if @url.save
      flash[:notice] = "Success!"
      redirect_to action: :show, token: @url.token, redirect: "no"
    end
  end

  def permitted_params
    params.require(:url).permit(:long_url)
  end

  def url_scope
    Url.all
  end

end
